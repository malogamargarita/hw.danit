"use strict";
let firstNumber;
do {
   firstNumber = prompt("Enter the first number");
} while (!firstNumber || typeof +firstNumber !== "number" || isNaN(+firstNumber) || firstNumber.trim() === "");
let secondNumber;
do {
   secondNumber = prompt("Enter the second number");
} while (!secondNumber || typeof +secondNumber !== "number" || isNaN(+secondNumber) || secondNumber.trim() === "");
let operation = prompt("Choose the operation:  + , - , * , /");
switch (operation) {
    case "+":
        console.log(`Result: ${ plus(+firstNumber,+secondNumber)}`);
        break;
    case "-":
        console.log(`Result: ${ minus(+firstNumber,+secondNumber)}`);
        break;
    case "*":
        console.log(`Result: ${multiplication(+firstNumber,+secondNumber)}`);
        break;
    case "/":
        console.log(`Result: ${division(+firstNumber,+secondNumber)}`);
        break;
    default:
        console.log("This operation is not exist!");
}
function plus(a=0,b=0){
    return a+b;
}
function minus(a=0,b=0){
    return a-b;
}
function multiplication(a=0,b=0){
    return a*b;
}
function division(a=0,b=0){
    return a/b;
}